
class Fighter {

    // name = '' health, attack, defense


    getHitPower(attack) {
        var criticalHitChance = this.randomInteger(1, 2);
        return attack * criticalHitChance;
    }


    getBlockPower(defense) {
        var dodgeChance = this.randomInteger(1, 2);
        return defense * dodgeChance;
    }


     randomInteger(min, max) {
        var rand = min - 0.5 + Math.random() * (max - min + 1);
        rand = Math.round(rand);
        return rand;
    }


    fight(fighter1, fighter2) {

        fighter1.power = 5;
        fighter2.power = 2;

        var healtMinus = this.getHitPower(fighter1.power) - this.getBlockPower(fighter2.power);
        this.health(healtMinus);

    }






}

export default Fighter;
import FightersView from './fightersView';
import { fighterService } from './services/fightersService';

class App {
    constructor() {
        this.startApp();
    }

    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');

    async startApp() {
        try {
            App.loadingElement.style.visibility = 'visible';

            const fighters = await fighterService.getFighters();
            const fightersView = new FightersView(fighters);
            const fightersElement = fightersView.element;

            fighters.forEach(function(item, i, arr) {
                // let fighterAll = fighterService.getFighterDetails(item._id);
            });
            // console.log(fighterAll);

            // var id = 1;
            // const fighter = await fighterService.getFighterDetails(id);
            // console.log(fighter);

            App.rootElement.appendChild(fightersElement);
        } catch (error) {
            console.warn(error);
            App.rootElement.innerText = 'Failed to load data';
        } finally {
            App.loadingElement.style.visibility = 'hidden';
        }
    }
}

export default App;